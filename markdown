<em><strong>Transaction API</strong></em> for public use.


There are several API that ready to be used:
- [x] accountBalance
- [x] blockAccount
- [x] fundTransfer
- [x] inquiryStatusTransaction
- [x] purchase
- [x] unblockAccount
- [ ] testService (example on development)





![Image of SoftwareAGLogo ](https://www.openapis.org/wp-content/uploads/sites/3/2017/04/software_ag.png)
Simple Transaction API was made using Software AG Integration Server
[SoftwareAG](https://www.softwareag.com/)
